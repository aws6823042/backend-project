import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  @Get('CookDay')
  getCookBy() {
    return this.reportsService.getCookBy();
  }

  @Get('price')
  getSumPrice(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getSumPriceSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getSumPriceSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getSumPriceSearchByYear();
    }
    return this.reportsService.getSumPrice();
  }

  @Get('order')
  getSumOrder(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getSumOrderSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getSumOrderSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getSumOrderSearchByYear();
    }
    return this.reportsService.getSumOrder();
  }

  @Get('table')
  getCountTable(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getCountTableSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getCountTableSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getCountTableSearchByYear();
    }
    return this.reportsService.getCountTable();
  }

  @Get('topMenu')
  getTopMenu(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getTopMenuSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getTopMenuSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getTopMenuSearchByYear();
    }
    return this.reportsService.getTopMenu();
  }

  @Get('topChef')
  getTopChef(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getTopChefSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getTopChefSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getTopChefSearchByYear();
    }
    return this.reportsService.getTopChef();
  }

  @Get('topServe')
  getTopServe(
    @Query()
    query: {
      search?: string;
    },
  ) {
    if (query.search == 'day') {
      return this.reportsService.getTopServeSearchByDay();
    } else if (query.search == 'month') {
      return this.reportsService.getTopServeSearchByMonth();
    } else if (query.search == 'year') {
      return this.reportsService.getTopServeSearchByYear();
    }
    return this.reportsService.getTopServe();
  }

  @Get('price7day')
  getPrice7day() {
    return this.reportsService.getPrice7day();
  }

  @Get('price12month')
  getPrice12month() {
    return this.reportsService.getPrice12month();
  }

  // @Get('searchStockList')
  // getSearchStockList(
  //   @Query()
  //   query: {
  //     search?: string;
  //   },
  // ) {
  //   console.log(query.search);
  //   return this.reportsService.getStockByDate(query.search);
  // }
}
