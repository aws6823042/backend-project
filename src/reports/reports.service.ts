import { Injectable } from '@nestjs/common';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  getCookBy() {
    return this.dataSource.query(
      `SELECT fname,lname,DATE_FORMAT(queue.createdAt, '%d/%m/%Y') AS date, count(*) as count FROM queue  INNER JOIN user ON queue.empCookId = user.id where status = 'เสิร์ฟสำเร็จ' and day(queue.createdAt) = day(curdate()) group by empCookId order by count desc`,
    );
  }

  getSumPriceSearchByDay() {
    return this.dataSource.query(
      `SELECT SUM(netPrice) AS sum_amount FROM payment WHERE YEAR(createdAt) = YEAR(CURDATE()) AND MONTH(createdAt) = MONTH(CURDATE()) AND DAY(createdAt) = DAY(CURDATE())`,
    );
  }
  getSumPriceSearchByMonth() {
    return this.dataSource.query(
      `SELECT SUM(netPrice) AS sum_amount FROM payment WHERE YEAR(createdAt) = YEAR(CURDATE()) AND MONTH(createdAt) = MONTH(CURDATE())`,
    );
  }
  getSumPriceSearchByYear() {
    return this.dataSource.query(
      `SELECT SUM(netPrice) AS sum_amount FROM payment WHERE YEAR(createdAt) = YEAR(CURDATE())`,
    );
  }
  getSumPrice() {
    return this.dataSource.query(
      `SELECT SUM(netPrice) AS sum_amount FROM payment`,
    );
  }

  getSumOrderSearchByDay() {
    return this.dataSource.query(
      `SELECT SUM(numConfirm) AS sum_Order FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) AND MONTH(orderitem.createdAt) = MONTH(CURDATE()) AND DAY(orderitem.createdAt) = DAY(CURDATE()) and menu.type != 'เครื่องดื่ม'`,
    );
  }
  getSumOrderSearchByMonth() {
    return this.dataSource.query(
      `SELECT SUM(numConfirm) AS sum_Order FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) AND MONTH(orderitem.createdAt) = MONTH(CURDATE()) and menu.type != 'เครื่องดื่ม'`,
    );
  }
  getSumOrderSearchByYear() {
    return this.dataSource.query(
      `SELECT SUM(numConfirm) AS sum_Order FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) and menu.type != 'เครื่องดื่ม'`,
    );
  }
  getSumOrder() {
    return this.dataSource.query(
      `SELECT SUM(numConfirm) AS sum_Order FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id where menu.type != 'เครื่องดื่ม'`,
    );
  }

  getCountTableSearchByDay() {
    return this.dataSource.query(
      `SELECT count(*) AS count FROM project_lazy.order WHERE YEAR(createdAt) = YEAR(CURDATE()) AND MONTH(createdAt) = MONTH(CURDATE()) AND DAY(createdAt) = DAY(CURDATE())`,
    );
  }
  getCountTableSearchByMonth() {
    return this.dataSource.query(
      `SELECT count(*) AS count FROM project_lazy.order WHERE YEAR(createdAt) = YEAR(CURDATE()) AND MONTH(createdAt) = MONTH(CURDATE())`,
    );
  }
  getCountTableSearchByYear() {
    return this.dataSource.query(
      `SELECT count(*) AS count FROM project_lazy.order WHERE YEAR(createdAt) = YEAR(CURDATE())`,
    );
  }

  getCountTable() {
    return this.dataSource.query(
      `SELECT count(*) AS count FROM project_lazy.order`,
    );
  }

  getTopMenuSearchByDay() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY SUM(numConfirm) DESC) AS id, name, SUM(numConfirm) AS sum FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) AND MONTH(orderitem.createdAt) = MONTH(CURDATE()) AND DAY(orderitem.createdAt) = DAY(CURDATE()) GROUP BY name ORDER BY sum DESC`,
    );
  }
  getTopMenuSearchByMonth() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY SUM(numConfirm) DESC) AS id, name, SUM(numConfirm) AS sum FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) AND MONTH(orderitem.createdAt) = MONTH(CURDATE()) GROUP BY name ORDER BY sum DESC`,
    );
  }
  getTopMenuSearchByYear() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY SUM(numConfirm) DESC) AS id, name, SUM(numConfirm) AS sum FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id WHERE YEAR(orderitem.createdAt) = YEAR(CURDATE()) GROUP BY name ORDER BY sum DESC`,
    );
  }

  getTopMenu() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY SUM(numConfirm) DESC) AS id, name, SUM(numConfirm) AS sum FROM orderitem INNER JOIN menu ON orderitem.menuId = menu.id GROUP BY name ORDER BY sum DESC`,
    );
  }

  getTopChefSearchByDay() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empCookId = user.id where status = 'เสิร์ฟสำเร็จ' and YEAR(queue.createdAt) = YEAR(CURDATE()) AND MONTH(queue.createdAt) = MONTH(CURDATE()) AND DAY(queue.createdAt) = DAY(CURDATE()) group by empCookId order by count desc`,
    );
  }
  getTopChefSearchByMonth() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empCookId = user.id where status = 'เสิร์ฟสำเร็จ' and YEAR(queue.createdAt) = YEAR(CURDATE()) AND MONTH(queue.createdAt) = MONTH(CURDATE()) group by empCookId order by count desc`,
    );
  }
  getTopChefSearchByYear() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empCookId = user.id where status = 'เสิร์ฟสำเร็จ' and YEAR(queue.createdAt) = YEAR(CURDATE()) group by empCookId order by count desc`,
    );
  }
  getTopChef() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empCookId = user.id where status = 'เสิร์ฟสำเร็จ' group by empCookId order by count desc`,
    );
  }

  getTopServeSearchByDay() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empServeId = user.id where YEAR(queue.createdAt) = YEAR(CURDATE()) AND MONTH(queue.createdAt) = MONTH(CURDATE()) AND DAY(queue.createdAt) = DAY(CURDATE()) group by empServeId order by count desc`,
    );
  }
  getTopServeSearchByMonth() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empServeId = user.id where YEAR(queue.createdAt) = YEAR(CURDATE()) AND MONTH(queue.createdAt) = MONTH(CURDATE()) group by empServeId order by count desc`,
    );
  }
  getTopServeSearchByYear() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empServeId = user.id where YEAR(queue.createdAt) = YEAR(CURDATE()) group by empServeId order by count desc`,
    );
  }
  getTopServe() {
    return this.dataSource.query(
      `SELECT ROW_NUMBER() OVER (ORDER BY count(*) DESC) AS id,fname,lname, count(*) as count FROM queue  INNER JOIN user ON queue.empServeId = user.id group by empServeId order by count desc`,
    );
  }

  getPrice7day() {
    return this.dataSource.query(
      `SELECT SUM(netPrice) AS sum, DAYNAME(createdAt) AS day FROM payment WHERE createdAt >= DATE_SUB(NOW(), INTERVAL 7 DAY) GROUP BY DAYNAME(createdAt) ORDER BY createdAt DESC`,
    );
  }

  getPrice12month() {
    return this.dataSource.query(
      `SELECT MONTHNAME(createdAt) AS month, SUM(netprice) AS sum FROM payment WHERE createdAt >= DATE_SUB(NOW(), INTERVAL 12 MONTH) GROUP BY MONTHNAME(createdAt) ORDER BY MONTH(createdAt) ASC`,
    );
  }

  // getStockByDate(date: string) {
  //   return this.dataSource.query(
  //     `SELECT check_stock_list.id,check_stock_list.createdAt,check_stock_list.updatedAt,check_stock_list.deletedAt, JSON_OBJECT('id', user.id,'fname', user.fname,'lname', user.lname,'type', user.type,'rate', user.rate,'gender', user.gender,'tel', user.tel,'image', user.image,'username', user.username,'password', user.password,'createdAt', user.createdAt,'updatedAt', user.updatedAt,'deletedAt', user.deletedAt) AS user FROM project_lazy.check_stock_list JOIN project_lazy.user ON user.id = check_stock_list.userId WHERE DATE(check_stock_list.createdAt) = '${date}'`,
  //   );
  // }
}
