import { StockBuyList } from 'src/stock-buy-list/entities/stock-buy-list.entity';

export class CreateStockBuyDetailDto {
  name: string;

  store: string;

  unit: string;

  price: number;

  stockBuyList: StockBuyList;
}
